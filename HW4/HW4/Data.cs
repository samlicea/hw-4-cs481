﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW4
{

    // Data class that contains the information type per each custom cell
    public class Data
    {
        public string Name { get; set; }
        public string Party { get; set; }
        public string Image { get; set; }
    }
}
