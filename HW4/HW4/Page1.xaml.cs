﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


/* Project was supposed to show the results from Super Tuesday in a ViewCell model
 * Each Cell contains (among states that participated)
 *  - Name of state
 *  - Their political lean (democrat or republican)
 *  - Picture of state
 * 
 * Every time a user tapped on a cell it would push a page containing the winning candidate
 * 
 * Need to implement
 * - Tap events per cell
 * - Refresh not working after first refresh
 * - Images inside cells not showing correctly
 * - Colors to individual cells 
 * - MenuItem (currently shows, but no functionality)
 */



namespace HW4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {

        // Observable Collection derived from Data class
        private ObservableCollection<Data> States;
        public Page1()
        {
            InitializeComponent();
            PopulateList();
        }

        // Func that create the list of states
        private void PopulateList()
        {
            States = new ObservableCollection<Data>();

            // Need to implement what page to call when individual cells are tapped

            // Data for cells
            var entry1 = new Data()
            {
                Name = "Alabama",
                Party = "Republican",
                Image = "Alabama.png",
                
            };

            var entry2 = new Data()
            {
                Name = "Arkansas",
                Party = "Republican",
                Image = "Arkansas.png",
            };

            var entry3 = new Data()
            {
                Name = "California",
                Party = "Democrat",
                Image = "CA.png",
            };

            var entry4 = new Data()
            {
                Name = "Colorado",
                Party = "Democrat",
                Image = "Colorado.png",
            };

            var entry5 = new Data()
            {
                Name = "Maine",
                Party = "Democrat",
                Image = "Maine.png",
            };

            var entry6 = new Data()
            {
                Name = "Massachusetts",
                Party = "Democrat",
                Image = "Massachusetts.png",
            };

            var entry7 = new Data()
            {
                Name = "Minnesota",
                Party = "Democrat",
                Image = "Minnesota.png",
            };

            var entry8 = new Data()
            {
                Name = "North Carolina",
                Party = "Democrat",
                Image = "NorthCarolina.png",
            };

            var entry9 = new Data()
            {
                Name = "Oklahoma",
                Party = "Republican",
                Image = "Oklahoma.png",
            };

            var entry10 = new Data()
            {
                Name = "Tennessee",
                Party = "Republican",
                Image = "Tennessee.png",
            };

            var entry11 = new Data()
            {
                Name = "Texas",
                Party = "Republican",
                Image = "Texas.png",
            };

            var entry12 = new Data()
            {
                Name = "Utah",
                Party = "Republican",
                Image = "Utah.png",
            };

            var entry13 = new Data()
            {
                Name = "Vermont",
                Party = "Democrat",
                Image = "Vermont.png",
            };

            var entry14 = new Data()
            {
                Name = "Virginia",
                Party = "Republican",
                Image = "Virnigia.png",
            };

            // Adds data to States List
            States.Add(entry1);
            States.Add(entry2);
            States.Add(entry3);
            States.Add(entry4);
            States.Add(entry5);
            States.Add(entry6);
            States.Add(entry7);
            States.Add(entry8);
            States.Add(entry9);
            States.Add(entry10);
            States.Add(entry11);
            States.Add(entry12);
            States.Add(entry13);
            States.Add(entry14);
            lstView.ItemsSource = States;
        }

        // handles refresh page
        private void LstView_Refreshing(object sender, System.EventArgs e)
        {
            PopulateList();
            lstView.IsPullToRefreshEnabled = false;
        }

        // Clicked event - Deletes Cell
        private void MenuItem_Clicked(object sender, System.EventArgs e)
        {

        }

        // Clicked event - shows who won Super Tuesday
        private void MenuItem_Clicked_1(object sender, EventArgs e)
        {

        }
    }

}